from marshmallow import Schema, fields, post_load
from marshmallow.validate import Range, OneOf


class TaskSchema(Schema):
    description = fields.Str(allow_none=False, required=True)
    active = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max_inclusive=1,
                                                             error='Некорректный флаг'), required=True)
    max_days = fields.Integer(allow_none=False, validate=Range(min=0, max=32, error='Некорректное число дней'), required=True)
    max_calls = fields.Integer(allow_none=False, validate=Range(min=0, max=32, error='Некорректное число звонков'), required=True)
    max_channels = fields.Integer(allow_none=False, validate=Range(min=0, max=32, error='Некорректное число каналов'), required=True)
    timeout = fields.Integer(allow_none=False, validate=Range(min=0, max=256, error='Некорректный таймаут'), required=True)
    success_duration = fields.Integer(allow_none=False, validate=Range(
        min=0, max=64, error='Некорректная длительность успешного звонка'), required=True)
    trunkid = fields.Str(allow_none=False, required=True)
    start_sound = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max_inclusive=1,
                                                             error='Некорректный флаг'), required=True)
    end_sound = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max_inclusive=1,
                                                             error='Некорректный флаг'), required=True)
    fax = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max_inclusive=1,
                                                             error='Некорректный флаг'), required=True)
    exten = fields.Str(allow_none=False, required=True)
    exten_type = fields.Integer(allow_none=False, validate=Range(min=0, max=8, error='Некорректный тип переадресации'), required=True)
    timeout = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max=8,
                                                              error='Некорректный тип переадресации'), required=True)
    predictive_mode = fields.Str(allow_none=False, validate=OneOf(['preview', 'none', 'static', 'auto'],
                                                                  error='Некорректный режим предиктивности'), required=True)
    predictive_percent = fields.Integer(allow_none=False, validate=Range(
        min=0, max=64, error='Некорректный коэффициент предиктивности'), required=True)

    day_1_start = fields.Time(allow_none=False, required=True)
    day_1_end = fields.Time(allow_none=False, required=True)
    day_2_start = fields.Time(allow_none=False, required=True)
    day_2_end = fields.Time(allow_none=False, required=True)
    day_3_start = fields.Time(allow_none=False, required=True)
    day_3_end = fields.Time(allow_none=False, required=True)
    day_4_start = fields.Time(allow_none=False, required=True)
    day_4_end = fields.Time(allow_none=False, required=True)
    day_5_start = fields.Time(allow_none=False, required=True)
    day_5_end = fields.Time(allow_none=False, required=True)
    day_6_start = fields.Time(allow_none=False, required=True)
    day_6_end = fields.Time(allow_none=False, required=True)
    day_7_start = fields.Time(allow_none=False, required=True)
    day_7_end = fields.Time(allow_none=False, required=True)

    hidden = fields.Integer(allow_none=False, validate=Range(min_inclusive=0, max_inclusive=1,
                                                             error='Некорректный флаг'), required=True)

    @post_load
    def return_data(self, data, **kwargs):
        return data
