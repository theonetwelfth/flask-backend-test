from sqlalchemy import create_engine, orm
from sqlalchemy.exc import InternalError
from sqlalchemy.ext.declarative import declarative_base

from config import DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASSWORD

engine = create_engine(f'mysql+pymysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}')
try:
    engine.execute(f'USE {DB_NAME};')
except InternalError:
    engine.execute(f'CREATE DATABASE {DB_NAME};')
    engine.execute(f'USE {DB_NAME};')

Base = declarative_base()
Session = orm.scoped_session(orm.sessionmaker())
Session.configure(bind=engine)
session = Session()
