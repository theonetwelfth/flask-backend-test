from sqlalchemy import Column
from sqlalchemy.dialects import mysql

from database import Base, session
from schema import TaskSchema


class Task(Base):
    __tablename__ = 'tasks'
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_comment': 'Таблица заданий на обзвон',
        'mysql_charset': 'utf8'
    }

    def __init__(self, data):
        TaskSchema().load(data)
        super(Task, self).__init__(**data)

    id = Column(mysql.INTEGER(10, unsigned=True), nullable=False, comment='ИД задания', primary_key=True)
    description = Column(mysql.VARCHAR(255), nullable=False, comment='Описание задания')
    active = Column(mysql.TINYINT(1), nullable=False, comment='Флаг активности задания')

    max_days = Column(mysql.SMALLINT(5, unsigned=True), nullable=False,
                      comment='Максимальное кол-во дней обзвона')
    max_calls = Column(mysql.SMALLINT(5, unsigned=True), nullable=False,
                       comment='Максимальное кол-во звонков в день')
    max_channels = Column(mysql.SMALLINT(5, unsigned=True), nullable=False,
                          comment='Максимальное кол-во используемых каналов')

    timeout = Column(mysql.MEDIUMINT(8, unsigned=True), nullable=False,
                     comment='Минимальный интервал между повторами звонков (в мин.)')
    success_duration = Column(mysql.SMALLINT(6, unsigned=True), nullable=False,
                              comment='Длительность успешного звонка в сек.')

    trunkid = Column(mysql.VARCHAR(25), nullable=False, comment='ИД транка FreePBX')
    start_sound = Column(mysql.TINYINT(1), nullable=False, comment='Есть ли запись приветствия')
    end_sound = Column(mysql.TINYINT(1), nullable=False, comment='Есть ли запись информации')
    fax = Column(mysql.TINYINT(1), nullable=False, comment='Есть ли факс')

    exten = Column(mysql.VARCHAR(25), nullable=False, comment='Вн. номер для перевода звонков')
    exten_type = Column(mysql.TINYINT(3, unsigned=True), nullable=False, comment='Тип переадресации')

    predictive_mode = Column(mysql.SET('preview', 'none', 'static', 'auto'), nullable=False,
                             default='none', comment='Режим предиктивности')
    predictive_percent = Column(mysql.SMALLINT(6), nullable=False, comment='Текущий коэффициент предиктивности')

    day_1_start = Column(mysql.TIME(), nullable=False)
    day_1_end = Column(mysql.TIME(), nullable=False)
    day_2_start = Column(mysql.TIME(), nullable=False)
    day_2_end = Column(mysql.TIME(), nullable=False)
    day_3_start = Column(mysql.TIME(), nullable=False)
    day_3_end = Column(mysql.TIME(), nullable=False)
    day_4_start = Column(mysql.TIME(), nullable=False)
    day_4_end = Column(mysql.TIME(), nullable=False)
    day_5_start = Column(mysql.TIME(), nullable=False)
    day_5_end = Column(mysql.TIME(), nullable=False)
    day_6_start = Column(mysql.TIME(), nullable=False)
    day_6_end = Column(mysql.TIME(), nullable=False)
    day_7_start = Column(mysql.TIME(), nullable=False)
    day_7_end = Column(mysql.TIME(), nullable=False)

    hidden = Column(mysql.TINYINT(1), nullable=False)

    def add(self):
        session.add(self)
        session.commit()

    def update(self):
        session.commit()

    def set_active(self, active_flag):
        self.active = active_flag
        session.commit()
