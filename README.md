# Flask-backend-test

Тестовое задание на позицию Flask-разработчика.  
Включает модель Task, схему для ее валидации при помощи marshmallow, а также несколько элементарных тестов для демонстрации работоспособности.