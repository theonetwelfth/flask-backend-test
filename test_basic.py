import pytest
from marshmallow.exceptions import ValidationError

from database import session, Session
from models import Task

BASE_OBJECT_DICT = {'description': 'abc', 'active': 0, 'max_days': 3, 'max_calls': 3, 'max_channels': 3, 'timeout': 5,
                    'success_duration': 8, 'trunkid': 'eqe', 'start_sound': 0, 'end_sound': 0, 'fax': 0,
                    'exten': '+79230000000', 'exten_type': 2, 'predictive_mode': 'none', 'predictive_percent': 8,
                    'day_1_start': '00:00:00', 'day_1_end': '23:59:59', 'day_2_start': '00:00:00',
                    'day_2_end': '23:59:59', 'day_3_start': '00:00:00', 'day_3_end': '23:59:59',
                    'day_4_start': '00:00:00', 'day_4_end': '23:59:59', 'day_5_start': '00:00:00',
                    'day_5_end': '23:59:59', 'day_6_start': '00:00:00', 'day_6_end': '23:59:59',
                    'day_7_start': '00:00:00', 'day_7_end': '23:59:59', 'hidden': 0}


# Smoke test (не задымилось - значит работает)
def test_smoke():
    Task(BASE_OBJECT_DICT).add()


# Тест модификации объекта
def test_modification():
    task = Task(BASE_OBJECT_DICT)
    task.description = 'def'
    task.update()


# Тест числовой валидации (на примере max_days)
def test_numeric_validation():
    with pytest.raises(ValidationError):
        test_dict = dict(BASE_OBJECT_DICT)
        test_dict['max_days'] = 64
        task = Task(test_dict)


# Тест структурной валидации (поле description отсутствует)
def test_integrity_validation():
    with pytest.raises(ValidationError):
        test_dict = dict(BASE_OBJECT_DICT)
        del test_dict['description']
        task = Task(test_dict)


# Тест валидации сета (predictive_mode)
def test_set_validation():
    with pytest.raises(ValidationError):
        test_dict = dict(BASE_OBJECT_DICT)
        test_dict['predictive_mode'] = 'walrus'
        task = Task(test_dict)


# Тест на нахождение объекта в базе
def test_object_existence():
    session.execute(f'TRUNCATE TABLE {Task.__tablename__};')
    Task(BASE_OBJECT_DICT).add()
    new_session = Session()
    task = new_session.query(Task).get(1)
    assert task.description == 'abc'
